//
//  LGuideAppDelegate.h
//  LGuide
//
//  Created by Kevin on 3/18/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGuideAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
