//
//  LGuideGuideDetail2ViewController.h
//  LGuide
//
//  Created by Kevin on 3/23/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGuideDetail2PagesViewController.h"

@interface LGuideGuideDetail2ViewController : UIPageViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSNumber *cellIndex;

@end
