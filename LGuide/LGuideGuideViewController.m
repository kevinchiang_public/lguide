//
//  LGuideGuideViewController.m
//  LGuide
//
//  Created by Kevin on 3/18/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideGuideViewController.h"
#import "LGuideGuideDetail1ViewController.h"
#import "LGuideLoginViewController.h"
#import <Parse/Parse.h>

@interface LGuideGuideViewController ()

@property (strong, nonatomic) NSMutableArray *dataArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) LGuideGuideDetail1ViewController *top;
@end

@implementation LGuideGuideViewController

- (NSMutableArray*) dataArray{
    if (!_dataArray) _dataArray = [[NSMutableArray alloc] init];
    return _dataArray;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    
    
    //First section data
    NSArray *firstItemsArray = [[NSArray alloc] initWithObjects:@"Cars", @"Living", @"Banking", nil];
    NSDictionary *firstItemsArrayDict = [NSDictionary dictionaryWithObject:firstItemsArray forKey:@"data"];
    [self.dataArray addObject:firstItemsArrayDict];
    
    //Second section data
    NSArray *secondItemsArray = [[NSArray alloc] initWithObjects:@"Malls", @"Asian Grocers", @"American Grocers", @"Other", nil];
    NSDictionary *secondItemsArrayDict = [NSDictionary dictionaryWithObject:secondItemsArray forKey:@"data"];
    [self.dataArray addObject:secondItemsArrayDict];
    
    NSArray *thirdItemsArray = [[NSArray alloc] initWithObjects:@"Chinese", @"Japanese", @"Korean", @"Vietnamese", @"American", @"Other Restaurants", nil];
    NSDictionary *thirdItemsArrayDict = [NSDictionary dictionaryWithObject:thirdItemsArray forKey:@"data"];
    [self.dataArray addObject:thirdItemsArrayDict];
    

    
    //self.canDisplayBannerAds = YES;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (![PFUser currentUser].username) { // No user logged in
        NSLog(@"No user logged in, proceeding to login");
        //Create the log in view controller
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        //LGuideLoginViewController *logInViewController = [[LGuideLoginViewController alloc] init];
        
        logInViewController.delegate = self;
        logInViewController.signUpController.delegate = self;
        
        logInViewController.fields =    PFLogInFieldsUsernameAndPassword |
                                        PFLogInFieldsPasswordForgotten |
                                        PFLogInFieldsSignUpButton;
        
        // Create the sign up view controller
        //PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        //[signUpViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Assign our sign up controller to be displayed from the login controller
        //[logInViewController setSignUpController:signUpViewController];
        
        // Present the log in view controller
        [self presentViewController:logInViewController animated:YES completion:nil];
    }else{
        //NSLog([NSString stringWithFormat:@"Current User: %s", [[PFUser currentUser].username UTF8String]]);
    }
}



// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Please enter your username and password."
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
}

//Table View Delegates

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //Number of rows it should expect should be based on the section
    NSDictionary *dictionary = [self.dataArray objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"data"];
    return [array count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if(section == 0){
        return @"Getting Started";
    }else if(section == 1){
        return @"Shopping!";
    }else{
        return @"Restaurants!";
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if(section == 2){
        return nil;
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *dictionary = [self.dataArray objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"data"];
    NSString *cellValue = [array objectAtIndex:indexPath.row];
    cell.textLabel.text = cellValue;
    //Subtitle Here
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%dth item",indexPath.row+1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictionary = [self.dataArray objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"data"];
    NSString *selectedCell = [array objectAtIndex:indexPath.row];
    
    //NSLog(@"%@", selectedCell);
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.top setNavBarTitle:selectedCell];
    self.top = nil;
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"guideDetail1"]){
        UINavigationController *nav = [segue destinationViewController];
        self.top = (LGuideGuideDetail1ViewController*) nav;
    }
}

@end
