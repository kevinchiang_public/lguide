//
//  LGuideAccountViewController.m
//  LGuide
//
//  Created by Kevin on 3/18/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideAccountViewController.h"
#import <Parse/Parse.h>
#import "LGuideTabBarController.h"

@interface LGuideAccountViewController ()
@property (weak, nonatomic) IBOutlet UIButton *logOutButton;
@property (strong, nonatomic) IBOutlet UILabel *username;

@end

@implementation LGuideAccountViewController

- (IBAction)logOut:(UIButton *)sender {
    [PFUser logOut];
    NSLog(@"User logged out.  Confirm: %@", [PFUser currentUser].username);
    [self.tabBarController setSelectedIndex:0];
    
}


-(UILabel *)username{
    if (!_username) _username = [[UILabel alloc] init];
    return _username;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSString *name =[PFUser currentUser].username;
    [self.username setText:[NSString stringWithFormat:@"Account:  %@", name]];
    }

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *blueButton = [[UIImage imageNamed:@"blueButton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(18,18,18,18)];
    UIImage *blueButtonHighlight = [[UIImage imageNamed:@"blueButtonHighlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [self.logOutButton setBackgroundImage:blueButton forState:UIControlStateNormal];
    [self.logOutButton setBackgroundImage:blueButtonHighlight forState:UIControlStateHighlighted];
    [self.logOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
