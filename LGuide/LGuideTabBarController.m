//
//  LGuideTabBarController.m
//  LGuide
//
//  Created by Kevin on 3/18/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideTabBarController.h"

@interface LGuideTabBarController ()

@end

@implementation LGuideTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



@end
