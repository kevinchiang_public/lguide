//
//  LGuideGuideDetail2ViewController.m
//  LGuide
//
//  Created by Kevin on 3/23/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideGuideDetail2ViewController.h"

@interface LGuideGuideDetail2ViewController ()
//@property (weak, nonatomic) IBOutlet UINavigationItem *navTitle;

@end

@implementation LGuideGuideDetail2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self scrollView];

   // NSLog(@"%@%d",self.data, [self.cellIndex intValue]);
    
    //_tempTest = [coordinates copy];
    
    // Create page view controller
    //self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];;
    self.automaticallyAdjustsScrollViewInsets = false;
    self.dataSource = self;
    LGuideDetail2PagesViewController *startingViewController = [self viewControllerAtIndex:[self.cellIndex integerValue]];
    NSArray *viewControllers = @[startingViewController];
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    
    // Change the size of page view controller
    
    
    //[self addChildViewController:_pageViewController];
    //[self.view addSubview:_pageViewController.view];
    //[self.pageViewController didMoveToParentViewController:self];
    

    //self.navTitle.title = [self.data[[self.cellIndex integerValue]] objectForKey:@"name"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//Data Source Protocols
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSUInteger index = ((LGuideDetail2PagesViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = ((LGuideDetail2PagesViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.data count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (LGuideDetail2PagesViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.data count] == 0) || (index >= [self.data count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    LGuideDetail2PagesViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detail2Pages"];
    pageContentViewController.data = self.data[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

@end



















