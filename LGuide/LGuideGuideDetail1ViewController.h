//
//  LGuideGuideDetail1ViewController.h
//  LGuide
//
//  Created by Kevin on 3/19/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGuideGuideDetail2ViewController.h"

@interface LGuideGuideDetail1ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) NSString *navBarTitle;
@end
