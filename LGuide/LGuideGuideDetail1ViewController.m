//
//  LGuideGuideDetail1ViewController.m
//  LGuide
//
//  Created by Kevin on 3/19/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideGuideDetail1ViewController.h"
#import "AFNetworking.h"

static NSString * const BaseURLString = @"http://lguide-deployd.herokuapp.com/";

@interface LGuideGuideDetail1ViewController ()
@property (weak, nonatomic) IBOutlet UINavigationItem *navTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *placesArrayFromAFNetworking;
@property (strong, nonatomic) NSArray *finishedPlacesArray;
@property (strong, nonatomic) LGuideGuideDetail2ViewController *top;
@end

@implementation LGuideGuideDetail1ViewController
@synthesize navBarTitle = _navBarTitle;

- (NSString *) navBarTitle{
    if (!_navBarTitle) _navBarTitle = [[NSString alloc] init];
    return _navBarTitle;
}

- (void)setNavBarTitle:(NSString *)navBarTitle{
    _navBarTitle = navBarTitle;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) makeServerRequest{
    NSString *type = self.navBarTitle;
    NSString *targetURL =
        [NSString stringWithFormat:@"%@places?{\"type\":\"%@\",\"$sort\":{\"name\":1}}",
         BaseURLString,type];
    //NSLog(targetURL);
    
    NSURL *url = [NSURL URLWithString:[targetURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //NSLog([url absoluteString]);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //NSLog(request);
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.placesArrayFromAFNetworking = responseObject;
        
        NSLog(@"The Array: %@", self.placesArrayFromAFNetworking);
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Request Failed: %@, %@", error, error.userInfo);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                        message:@"We took too long loading this."
                                                       delegate:self
                                              cancelButtonTitle:@"Retry"
                                              otherButtonTitles:nil];
        // optional - add more buttons:
        [alert addButtonWithTitle:@"Yes"];
        [alert show];
        
    }];
    
    [operation start];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self makeServerRequest];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitle.title = self.navBarTitle;
    self.tableView.tableFooterView = [[UIView alloc] init];
    if (!self.placesArrayFromAFNetworking){
        [self makeServerRequest];
    }
    
    //Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:
                                      @"Pull to refresh."];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    //NSLog(@"Refreshing");
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:
    //                                 @"Refreshing data..."];
    
    [self makeServerRequest];
    [refreshControl endRefreshing];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString: @"Pull to Refresh."];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog([NSString stringWithFormat:@"%d",[self.placesArrayFromAFNetworking count]]);
    return [self.placesArrayFromAFNetworking count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"placeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *tempDictionary= [self.placesArrayFromAFNetworking objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [tempDictionary objectForKey:@"name"];
    
    if([tempDictionary objectForKey:@"rating"] != NULL)
    {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Rating: %@",[tempDictionary   objectForKey:@"rating"]];
    }
    else
    {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Not Rated"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.top.cellIndex = [[NSNumber alloc ] initWithInt:indexPath.row];
    self.top.data = self.placesArrayFromAFNetworking;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"detail1toDetail2"]){
        UINavigationController *nav = [segue destinationViewController];
        self.top = (LGuideGuideDetail2ViewController*) nav;
    }
}

@end
