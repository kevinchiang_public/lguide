//
//  LGuideGuideViewController.h
//  LGuide
//
//  Created by Kevin on 3/18/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import <Parse/Parse.h>

@interface LGuideGuideViewController : UIViewController
    <UITableViewDelegate,UITableViewDataSource, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@end
