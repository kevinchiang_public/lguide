//
//  LGuideDetail2PagesViewController.h
//  LGuide
//
//  Created by Kevin on 3/23/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGuideDetail2PagesViewController : UIViewController

@property NSUInteger pageIndex;
@property (strong, nonatomic) NSDictionary *data;
@end
