//
//  LGuideDetail2PagesViewController.m
//  LGuide
//
//  Created by Kevin on 3/23/14.
//  Copyright (c) 2014 Kevin Chiang. All rights reserved.
//

#import "LGuideDetail2PagesViewController.h"
#import <MapKit/MapKit.h>

@interface LGuideDetail2PagesViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (strong, nonatomic) NSArray *latlong;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextView *moreInfo;
@property (weak, nonatomic) IBOutlet UIButton *phoneCallButton;
@end

#define METERS_PER_MILE 1609.344
@implementation LGuideDetail2PagesViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)call:(id)sender{
    NSString *phoneNumber = [NSString stringWithFormat:@"tel://%@",
                             [[self.data objectForKey:@"phoneNumber"]
                              stringByReplacingOccurrencesOfString:@"-"
                              withString:@""]] ;
    
    NSLog(@"%@",phoneNumber);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Buttons!
    UIImage *blueButton = [[UIImage imageNamed:@"blueButton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(18,18,18,18)];
    UIImage *blueButtonHighlight = [[UIImage imageNamed:@"blueButtonHighlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [self.phoneCallButton setBackgroundImage:blueButton forState:UIControlStateNormal];
    [self.phoneCallButton setBackgroundImage:blueButtonHighlight forState:UIControlStateHighlighted];
    [self.phoneCallButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleGesture:)];
    tgr.numberOfTapsRequired = 1;
    tgr.numberOfTouchesRequired = 1;
    [self.map addGestureRecognizer:tgr];
}


- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;
    
    // Create an MKMapItem to pass to the Maps app
    CLLocationCoordinate2D coordinate =
    CLLocationCoordinate2DMake([[self.data objectForKey:@"latlong"][0] doubleValue],
                               [[self.data objectForKey:@"latlong"][1] doubleValue]);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:[self.data objectForKey:@"name"]];
    
    // Set the directions mode to "Walking"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                   launchOptions:launchOptions];
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [[self.data objectForKey:@"latlong"][0] doubleValue];
    zoomLocation.longitude= [[self.data objectForKey:@"latlong"][1] doubleValue];

    MKPointAnnotation *addAnnotation = [[MKPointAnnotation alloc] init];
    addAnnotation.coordinate = zoomLocation;
    [_map addAnnotation:addAnnotation];
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_map setRegion:viewRegion animated:NO];
    self.name.text = [NSString stringWithFormat:@"%@",
                      [self.data objectForKey:@"name"]];
    
    self.moreInfo.text = [NSString stringWithFormat:@"%@",
                          //[self.data objectForKey:@"phoneNumber"],
                          [self.data objectForKey:@"address"]
                          ];
    
    [self.phoneCallButton setTitle:[NSString stringWithFormat:@"Call:  %@",
                                    [self.data objectForKey:@"phoneNumber"]
                                    ]
                          forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
